<?php
//** INSTALL & UNINSTALL *********************************************//
function redonp_install() {
  global $wpdb, $charset_collate;

  // check for capability
	if( !current_user_can( 'activate_plugins' ) ) 
		return;

  // create the database
  // I could probably do this with an Option but it's a pain in the ass to
  // keep iterating it manually all the damn time
  $jpmirrors = $wpdb->prefix . 'redonp_journals';
  //if( $wpdb->get_var( "show tables like '$jpmirrors'" ) != $jpmirrors ) {
      
		$sql = "CREATE TABLE " . $jpmirrors . " (
             `journalID` tinyint(3) unsigned NOT NULL auto_increment,
             `journalServer` varchar(150) NOT NULL,
             `journalUser` varchar(30) NOT NULL,
             `journalPass` varchar(32) NOT NULL,
             `journalComm` varchar(30) NOT NULL,
             `journalUse` enum('yes','ask','no') NOT NULL default 'ask',
             `journalPics` text,
             `journalPicDefault` varchar(255) NULL,
             PRIMARY KEY  (`journalID`),
             KEY `journalServer` (`journalServer`,`journalUser`,`journalComm`)
		       ) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_unicode_ci;";

	  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
  //}
  update_option( 'redonp_installed', '0.4' );
}

function redonp_uninstall(){
  global $wpdb;

  // do stuff in here in the future hey
  delete_option( 'redonp_installed' );
}