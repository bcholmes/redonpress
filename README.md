# RedonPress #

This is the source-code for a Wordpress plugin to cross-post content from a Wordpress
blog to a Dreamwidth (or other LiveJournal-style) account. This code is largely
scraped from an existing plugin called JournalPress, created by Alis Dee. Unfortunately,
JournalPress hasn't been updated in six years. So, there's that.

### What's with the name? ###

I like Dreamwidth, and was interested in a plug-in that emphasized a "dream" name.
Unfortunately, there's already a thing called "DreamPress" that's unrelated to
Dreamwidth.

I'm also a fan of the symbolist painter, Odilon Redon, who created a series of
paintings titled "In the Dream" ("Dans le Rêve").

### Did This Codebase Suddenly Become More Active? ###

Yeah, it looks like Dreamwidth started enforcing HTTPS on its XML-RPC API, and
JournalPress stopped working for me. So I brushed off the code and started updating it
to support HTTPS.

### Contact info ###

* Twitter [@bcholmesdotorg](https://twitter.com/bcholmesdotorg)
