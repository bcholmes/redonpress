<?php
//** SET UP THE ADMINISTRATION LINKS *********************************//

function redonp_admin_register_settings() {
	register_setting('discussion', 'redonp_require_commentform');
}

function redonp_links( $links ) {
 $settings_link = '<a href="./admin.php?page=redonp-options">Settings</a>';
 $journals_link = '<a href="./admin.php?page=redonpress">Journals</a>';
 array_unshift( $links, $settings_link );
 array_unshift( $links, $journals_link );
 return $links;
}
function redonp_add_pages() {
	add_menu_page( 'RedonPress Mirrors', 'RedonPress', 'administrator', 'redonpress', 'redonp_display_manage', WP_PLUGIN_URL . '/redonpress/icon.png' );

	add_submenu_page( 'redonpress', 'RedonPress Options', 'Options', 'administrator', 'redonp-options', 'redonp_display_options' );
	add_submenu_page( 'redonpress', 'Bulk Actions', 'Bulk Actions', 'administrator', 'redonp-bulk', 'redonp_display_bulk' );
	add_submenu_page( 'redonpress', 'RedonPress Credits', 'Credits', 'administrator', 'redonp-credits', 'redonp_display_credits' );

	add_meta_box( 'postjp', 'RedonPress', 'redonp_post_advanced', 'post', 'advanced', 'high' );

	// add OpenID options to Discussion Settings page
	add_settings_field('redonp_disucssion_settings', __('RedonPress Settings', 'redonpress'), 'redonp_discussion_settings', 'discussion', 'default');

}


//** PRINT THE ADMINISTRATION PAGES **********************************//
function redonp_display_options() {
	global $wpdb;

	// create the option variables
	add_option( 'redonp_custom_name' );
	add_option( 'redonp_privacy' );
	add_option( 'redonp_privacyl' );
	add_option( 'redonp_privacyp' );
	add_option( 'redonp_comments' );
	add_option( 'redonp_tag' );
	add_option( 'redonp_cat' );
	add_option( 'redonp_more' );
	add_option( 'redonp_header_loc' );
	add_option( 'redonp_custom_header' );

	// Retrieve these for the form
	$old_name = stripslashes(get_option('redonp_custom_name'));
	$old_privacy = get_option('redonp_privacy');
	$old_privacyl = get_option('redonp_privacyl');
	$old_privacyp = get_option('redonp_privacyp');
	$old_comments = get_option('redonp_comments');
	$old_tag = get_option('redonp_tag');
	$old_cat = get_option('redonp_cat');
	$old_more = get_option('redonp_more');
	$old_header_loc = get_option('redonp_header_loc');
	$old_custom_header = stripslashes( get_option( 'redonp_custom_header' ) );

  // default custom header
	if( $old_custom_header == '' ) {
	  $old_custom_header = '<p style="text-align: right"><small>Mirrored from <a href="[permalink]" title="Read Original Post">[blog_name]</a>.</small></p>';
		update_option( 'redonp_custom_header', stripslashes( $old_custom_header ) );
	}

	// default to public posts
	if( $old_privacy == '' ) {
		update_option( 'redonp_privacy', 'public' );
		$old_privacy = "public";
	}

	// default to private posts
	if( $old_privacyp == '' ) {
		update_option( 'redonp_privacyp', 'private' );
		$old_privacyp = "private";
	}

	// default to f-locked posts
	if( $old_privacyl == '' ) {
		update_option( 'redonp_privacyl', 'friends' );
		$old_privacyl = "friends";
	}

	// default to comments on the blog only
	if( $old_comments === '' ) {
		update_option( 'redonp_comments', 0 );
		$old_comments = 0;
	}

	// Default to allowing tags - only in strange i18n situations would you
	// want them disabled
	if( $old_tag === '' ) {
		update_option( 'redonp_tag', 1 );
		$old_tag = 1;
	}

	// The default option is to link back to the original site if there is a
	// <!--more--> tag
	if( $old_more == '' ) {
		update_option( 'redonp_more', 'link' );
		$old_more = 'link';
	}

	// 0 means top, 1 means bottom
	if( $old_header_loc == '' ) {
		update_option( 'redonp_header_loc', 1 );
		$old_header_loc = 1;
	}

	// If we're handling a submission, save the data
	if( isset( $_POST['update_lj_options'] ) ) {

		if( $old_name != $_POST['custom_name'] ) {
			update_option( 'redonp_custom_name', $_POST['custom_name'] );
			$old_name = $_POST['custom_name'];
		}

		if( $old_privacy != $_POST['privacy'] ){
			update_option('redonp_privacy', $_POST['privacy']);
			$old_privacy = $_POST['privacy'];
		}

		if( $old_privacyp != $_POST['privacyp'] ){
			update_option('redonp_privacyp', $_POST['privacyp']);
			$old_privacyp = $_POST['privacyp'];
		}

		if( $old_privacyl != $_POST['privacyl'] ){
			update_option('redonp_privacyl', $_POST['privacyl']);
			$old_privacyl = $_POST['privacyl'];
		}

		if( $old_comments != $_POST['comments'] ){
			update_option('redonp_comments', $_POST['comments']);
			$old_comments = $_POST['comments'];
		}

		if( $old_tag != $_POST['tag'] ) {
			update_option( 'redonp_tag', $_POST['tag'] );
			$old_tag = $_POST['tag'];
		}
		if( $old_cat != $_POST['cat'] ) {
			update_option( 'redonp_cat', $_POST['cat'] );
			$old_cat = $_POST['cat'];
		}

		if( $old_more != $_POST['more'] ) {
			update_option('redonp_more', $_POST['more']);
			$old_more = $_POST['more'];
		}

		if( $old_header_loc != $_POST['header_loc'] ){
			update_option( 'redonp_header_loc', $_POST['header_loc'] );
			$old_header_loc = $_POST['header_loc'];
		}

		if( $old_custom_header != $_POST['custom_header'] ) {
			update_option('redonp_custom_header', $_POST['custom_header']);
			$old_custom_header = $_POST['custom_header'];
		}

		echo '<div id="message" class="updated fade"><p>';
		_e( 'RedonPress options saved successfully!', JXP_DOMAIN );
		echo '</p></div>';
	}

?>
<div class="wrap">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<h2><?php _e( 'RedonPress Configuration', JXP_DOMAIN ); ?></h2>
		<p><?php _e( 'Please note that changes made here only affect <em>future</em> blog posts; they will not update old entries. To apply these changes to all previously crossposted posts, hit up the <a href="./admin.php?page=redonp-bulk">bulk actions</a> page.', JXP_DOMAIN ); ?></p>

		<h3><?php _e( 'Linkback', JXP_DOMAIN ); ?></h3>
		<table class="form-table">
		  <tr valign="top">
		  <!-- crosspost linkback location -->
			  <th width="33%" scope="row"><?php _e( 'Crosspost Linkback Location', JXP_DOMAIN ); ?></th>
				<td><label><input name="header_loc" type="radio" value="0" <?php
					if( 0 == $old_header_loc ) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Above post', JXP_DOMAIN ); ?></label><br />
				<label><input name="header_loc" type="radio" value="1" <?php
					if( 1 == $old_header_loc ) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Below post', JXP_DOMAIN ); ?></label></td>
			</tr>
			<!-- custom blog name for linkback -->
			<tr valign="top">
				<th scope="row"><?php _e( 'Custom Blog Title', JXP_DOMAIN ); ?></th>
				<td><input name="custom_name" type="text" id="custom_name" value="<?php echo htmlentities( stripslashes( $old_name ), ENT_COMPAT, 'UTF-8' ); ?>" size="40" />
				<p class="setting-description"><?php printf(__( 'If you wish your linkbaks to use a custom blog title, enter it here. Otherwise, the default name of your blog will be used (%s).', JXP_DOMAIN), get_settings('blogname') ); ?></p>
				</td>
			</tr>
			<!-- linkback format -->
			<tr valign="top">
				<th scope="row"><?php _e( 'Linkback Format', JXP_DOMAIN ); ?></th>
				<td><textarea name="custom_header" id="custom_header" rows="3" cols="40"><?php echo htmlentities( stripslashes( $old_custom_header ), ENT_COMPAT, 'UTF-8' ); ?></textarea><br />
				  <p class="setting-description"><?php _e( "If you don't like the default linkback format, specify your own here. For flexibility, you can choose from a series of case-sensitive substitution strings, listed below:", JXP_DOMAIN ); ?></p>
				  <dl class="setting-description">
  					<dt>[blog_name]</dt>
  					<dd><?php _e( 'The title of your blog, as specified above.', JXP_DOMAIN ); ?></dd>

					  <dt>[blog_link]</dt>
					  <dd><?php _e( 'The URL of your blog\'s homepage.', JXP_DOMAIN ); ?></dd>

					  <dt>[permalink]</dt>
					  <dd><?php _e( 'The blog\'s permalink for your post.', JXP_DOMAIN); ?></dd>

					  <dt>[comments_link]</dt>
				    <dd><?php _e( 'The URL for comments. Generally this is the permalink URL with #comments on the end.', JXP_DOMAIN ); ?></dd>
					</dl>
				</td>
		  </tr>
		</table>

		<h3><?php _e( 'Posting & Commenting', JXP_DOMAIN ); ?></h3>
		<table class="form-table">
		  <!-- default privacy level for posts -->
		  <tr valign="top">
		  <th scope="row"><?php _e('Default Privacy Level', JXP_DOMAIN); ?></th>
			<td><label><input name="privacy" type="radio" value="public" <?php
					if("public" == $old_privacy) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Public', JXP_DOMAIN); ?></label><br />
					<label><input name="privacy" type="radio" value="private" <?php
					if("private" == $old_privacy) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Private', JXP_DOMAIN); ?></label><br />
					<label><input name="privacy" type="radio" value="friends" <?php
					if("friends" == $old_privacy) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Friends only', JXP_DOMAIN); ?></label><br />
			  </td>
		  </tr>
		  <!-- default privacy level for private posts -->
		  <tr valign="top">
		  <th scope="row"><?php _e('Privacy Level: Private Posts', JXP_DOMAIN); ?></th>
			<td><label><input name="privacyp" type="radio" value="public" <?php
					if("public" == $old_privacyp) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Public', JXP_DOMAIN); ?></label><br />
					<label><input name="privacyp" type="radio" value="private" <?php
					if("private" == $old_privacyp) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Private', JXP_DOMAIN); ?></label><br />
					<label><input name="privacyp" type="radio" value="friends" <?php
					if("friends" == $old_privacyp) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Friends only', JXP_DOMAIN); ?></label><br />
					<label><input name="privacyp" type="radio" value="noxp" <?php
					if("noxp" == $old_privacyp) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Do not crosspost', JXP_DOMAIN); ?></label><br />
			  </td>
		  </tr>
		  <!-- default privacy level for password locked posts -->
		  <tr valign="top">
		  <th scope="row"><?php _e('Privacy Level: Password-Protected Posts', JXP_DOMAIN); ?></th>
			<td><label><input name="privacyl" type="radio" value="public" <?php
					if("public" == $old_privacyl) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Public', JXP_DOMAIN); ?></label><br />
					<label><input name="privacyl" type="radio" value="private" <?php
					if("private" == $old_privacyl) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Private', JXP_DOMAIN); ?></label><br />
					<label><input name="privacyl" type="radio" value="friends" <?php
					if("friends" == $old_privacyl) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Friends only', JXP_DOMAIN); ?></label><br />
					<label><input name="privacyl" type="radio" value="noxp" <?php
					if("noxp" == $old_privacyl) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e('Do not crosspost', JXP_DOMAIN); ?></label><br />
			  </td>
		  </tr>
		  <!-- default commenting options -->
		  <tr valign="top">
			  <th scope="row"><?php _e( 'Comment Options', JXP_DOMAIN ); ?></th>
			  <td><label><input name="comments" type="radio" value="0" <?php
					if(0 == $old_comments) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Require users to comment on the original entry.', JXP_DOMAIN ); ?></label><br />
					<label><input name="comments" type="radio" value="1" <?php
					if('1' == $old_comments) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Allow comments on all mirrored entries.', JXP_DOMAIN ); ?></label><br />
					<label><input name="comments" type="radio" value="2" <?php
					if('2' == $old_comments) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Allow comments on locked mirrored entries only.', JXP_DOMAIN ); ?></label><br />
        </td>
      </tr>
      <!-- tag mirrored entries? -->
		  <tr valign="top">
			  <th scope="row"><?php _e( 'Mirror Tags', JXP_DOMAIN ); ?></th>
				<td><label><input name="cat" type="checkbox" value="1" <?php
					if( $old_cat ) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Tag mirrored entries with WordPress categories.', JXP_DOMAIN ); ?></label><br />

					<label><input name="tag" type="checkbox" value="1" <?php
					if( $old_tag ) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Tag mirrored entries with WordPress tags.', JXP_DOMAIN ); ?></label><br />
				</td>
			</tr>
			<!-- cut text stuff -->
		  <tr valign="top">
			  <th scope="row"><?php _e( 'Handling of &lt;!--More--&gt;', JXP_DOMAIN ); ?></th>
				<td><label><input name="more" type="radio" value="link" <?php
					if("link" == $old_more) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Link back to this blog.', JXP_DOMAIN ); ?></label><br />
					<label><input name="more" type="radio" value="lj-cut" <?php
					if("lj-cut" == $old_more) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Use an lj-cut.', JXP_DOMAIN ); ?></label><br />
					<label><input name="more" type="radio" value="copy" <?php
					if("copy" == $old_more) {
						echo 'checked="checked" ';
					}
					?>/> <?php _e( 'Mirror the entire entry with no cuts.', JXP_DOMAIN); ?></label><br />
				</td>
			</tr>
		</table>

		<p class="submit">
			<input type="submit" name="update_lj_options" value="<?php _e('Update Options'); ?>" style="font-weight: bold;" />
		</p>

	</form>
</div>
<?php
}

function redonp_display_credits() {
?>
<div class="wrap">
  <h2><?php _e( 'RedonPress Credits & Readme', JXP_DOMAIN ); ?></h2>
  <h3><?php _e( 'Credits', JXP_DOMAIN ); ?></h3>
  <p><?php _e( '<strong>RedonPress</strong> is a fork of <a href="https://wordpress.org/plugins/journalpress/"><strong>JournalPress</strong></a>, originally written by <a href="https://alisfranklin.com/about/" title="Go ask Alis.">Alis Dee/Franklin</a>, but adapted by <a href="http://blog.bcholmes.org/">BC Holmes</a>. It is based off the original <a href="http://ebroder.net/livejournal-crossposter/">LJXP</a> client by Evan Broder, with the <a href="http://www.alltrees.org/Wordpress/#LCP">LJ Crossposter Plus</a> modifications made by Ravenwood and Irwin. No disrespect is intended towards any of these authors; without their great work, this plugin wouldn\'t have been possible (or at least would\'ve taken a hell of a lot longer to write).', JXP_DOMAIN ); ?></p>
  <p><?php _e( 'Batch update and delete journal code care of <a href="http://www.branchandroot.net/">branchandroot</a>.', JXP_DOMAIN ); ?></p>

  <h3><?php _e( 'Current Features', JXP_DOMAIN ); ?></h3>
  <p><?php _e( 'The main difference between JP and its predecessor plugins is that it allows crossposting to multiple journals. It also supports custom security settings via the <a href="http://void-star.net/projects/wp-flock">WP-Flock</a> plugin.', JXP_DOMAIN ); ?></p>

  <h3><?php _e( 'Feedback', JXP_DOMAIN ); ?></h3>
  <p><?php _e( 'Praise and kudos <a href="http://void-star.net/projects/redonpress/">here</a>, bug reports and other support problems to the <a href="https://bitbucket.org/bcholmes/redonpress/issues">issues tracker</a>.', JXP_DOMAIN ); ?></p>
</div>
<?php
}

function redonp_display_manage() {
	global $wpdb;

  // are we deleting a journal?
  if( !empty( $_POST['deleteit'] ) && !empty( $_POST['deljournal'] ) ){
    foreach( $_POST['deljournal'] as $jID )
      $wpdb->query( $wpdb->prepare( "DELETE FROM `$wpdb->jpmirrors` WHERE `journalID` = %d;", $jID ) );

    echo '<div id="message" class="updated fade"><p>';
    if( $r === FALSE ) {
      $wpdb->show_errors();
      _e( '  Error deleting journal!<br/>', JXP_DOMAIN );
      $wpdb->print_error();
      $wpdb->hide_errors();
    } else
		  _e( '  Journal(s) deleted successfully.', JXP_DOMAIN );
		echo '</p></div>';
  }

	// are we adding a new journal?
	if( !empty( $_POST['add_journal'] ) ) {
    // see if we can populate our userpics list
    $ljc = new DWClient( $_POST['redonp_username'], $_POST['redonp_pword'], $_POST['redonp_server'] );
    $response = $ljc->login();

    if( $response[0] === TRUE ) {
      $rsp = $response[1];
      $piclist = !empty( $rsp['pickws'] ) ? redonp_getUserPics( $rsp['pickws'] ) : '';
      if( $piclist && !empty( $rsp['defaultpicurl'] ) )
        $dpic = redonp_getDefaultPic( $rsp['pickws'], $rsp['pickwurls'], $rsp['defaultpicurl'] );
    }
    // stick it in our database!
    $r = $wpdb->query( $wpdb->prepare(
      "INSERT INTO `$wpdb->jpmirrors` ( `journalServer`, `journalUser`, `journalPass`, `journalComm`, `journalUse`, `journalPics`, `journalPicDefault` )  VALUES( %s, %s, %s, %s, %s, %s, %s );"
      ,$_POST['redonp_server']
      ,$_POST['redonp_username']
      ,$_POST['redonp_pword']
      ,$_POST['redonp_community']
      ,$_POST['redonp_use']
      ,$piclist
      ,$dpic
     ) );

    echo '<div id="message" class="updated fade"><p>';
    if( $r === FALSE ) {
      $wpdb->show_errors();
      _e( '  Error adding new journal mirror!<br/>', JXP_DOMAIN );
      $wpdb->print_error();
      $wpdb->hide_errors();
    } else
		  _e( '  New journal mirror added successfully.', JXP_DOMAIN );
		echo '</p></div>';
	}

	// are we editing an old journal
	if( !empty( $_POST['edit_journal'] ) ) {
    // update the database!
    if( empty( $_POST['redonp_pword'] ) )
      $usql = $wpdb->prepare( "UPDATE `$wpdb->jpmirrors` SET journalUse = %s, journalPics = %s, journalPicDefault = %s WHERE journalID = %d;", $_POST['redonp_use'], preg_replace( '/["\']/', '', $_POST['redonp_pics'] ), preg_replace( '/["\']/', '', $_POST['redonp_dpic'] ), $_GET['jID'] );
    else
      $usql = $wpdb->prepare( "UPDATE `$wpdb->jpmirrors` SET journalUse = %s, journalPass = %s, journalPics = %s, journalPicDefault = %s WHERE journalID = %d;", $_POST['redonp_use'], md5( $_POST['redonp_pword'] ), preg_replace( '/["\']/', '', $_POST['redonp_pics'] ), preg_replace( '/["\']/', '', $_POST['redonp_dpic'] ), $_GET['jID'] );
    $r = $wpdb->query( $usql );

    echo '<div id="message" class="updated fade"><p>';
    if( $r === FALSE ) {
      $wpdb->show_errors();
      _e( '  Error editing journal mirror!<br/>', JXP_DOMAIN );
      $wpdb->print_error();
      $wpdb->hide_errors();
    } else
		  _e( '  Journal mirror edited successfully.', JXP_DOMAIN );
		echo '</p></div>';
	}

	// show us our page
?>
<div class="wrap">
  <h2><?php _e( 'RedonPress Mirrors', JXP_DOMAIN ); ?></h2>

<?php if( empty( $_GET['jID'] ) ) { ?>
  <h3><?php _e( 'Curent Mirrors', JXP_DOMAIN ); ?></h3>

  <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
  <div class="tablenav">
  <div class="alignleft">
    <input type="submit" value="Delete" name="deleteit" class="button-secondary delete" />
  </div>

  <br class="clear" />
  </div>
  <br class="clear" />

  <table class="widefat">
  <thead>
    <tr>
	    <th scope="col" class="check-column">&nbsp;</th>
      <th style="width: 15%;">User</th><th>Server</th><th>Community</th><th style="text-align: center">Use?</th><th style="text-align: center">Edit</th>
    </tr>
	</thead>

	<tbody>
    <tr id="link-2" valign="middle">
<?php
  // get the existing table data
  $js = $wpdb->get_results( 'SELECT `journalID`, `journalServer`, `journalUser`, `journalComm`, `journalUse` FROM `'. $wpdb->jpmirrors .'` ORDER BY journalUse, journalServer, journalUser, journalComm;' );

  foreach( $js as $js ) {
    $comtxt = empty( $js->journalComm ) ? '--' : "in <a href='http://$js->journalServer/community/$js->journalComm' title='View Community'>$js->journalComm</a>";
    print "<tr><th scope=\"row\" class=\"check-column\"><input type=\"checkbox\" name=\"deljournal[]\" value=\"$js->journalID\" /></th>".
          "<td><strong><a class='row-title' href='http://$js->journalServer/users/$js->journalUser' title='View Journal'>$js->journalUser</a></strong></td>".
          "<td>$js->journalServer</td>".
          "<td>$comtxt</td>".
          "<td style='text-align: center;'>$js->journalUse</td>".
          "<td style='text-align: center;'><a href='./admin.php?page=redonpress&jID=$js->journalID'>edit</a></td>\n</tr>\n";
  }

  ?>
    </tr>
  </tbody>
  </table>
  </form>

    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<h3><?php _e( 'Add New Mirror', JXP_DOMAIN ); ?></h3>
		<table class="form-table">
		  <tr valign="top">
				<th scope="row"><?php _e( 'Server', JXP_DOMAIN ); ?></th>
				<td><input name="redonp_server" type="text" id="redonp_server" value="www.livejournal.com" size="40" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e( 'Username', JXP_DOMAIN ); ?></th>
				<td><input name="redonp_username" type="text" id="redonp_username" size="40" />
				</td>
			</tr>
		  <tr valign="top">
				<th scope="row"><?php _e( 'Password', JXP_DOMAIN ); ?></th>
				<td><input name="redonp_pword" type="password" id="redonp_pword" size="40" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php _e( 'Post In Community', JXP_DOMAIN ); ?></th>
				<td><input name="redonp_community" type="text" id="redonp_community" size="40" />
				  <p class="setting-description"><?php _e( 'If you wish to crosspost to a community, enter its name here. Otherwise, you may leave this field blank.', JXP_DOMAIN ); ?></p>
				</td>
			</tr>
		  <tr valign="top">
			  <th width="33%" scope="row"><?php _e( 'Use by Default', JXP_DOMAIN ); ?></th>
				<td><label><input name="redonp_use" type="radio" value="yes" /> <?php _e( 'Yes, mirror to this journal by default.', JXP_DOMAIN ); ?></label><br />
				<label><input name="redonp_use" type="radio" value="no" /> <?php _e( 'No, hide this journal from the mirror list.', JXP_DOMAIN ); ?></label><br />
				<label><input name="redonp_use" type="radio" value="ask" /> <?php _e( 'Ask me if I wish to use this journal, but do not post to it by default.', JXP_DOMAIN ); ?></label></td>
			</tr>
		</table>

		<p class="submit">
			<input type="submit" name="add_journal" value="<?php _e( 'Add New Mirror' ); ?>" style="font-weight: bold;" />
		</p>
	</form>
<?php } else { /* EDIT A JOURNAL'S OPTIONS */
  // get the existing table data
  $j = $wpdb->get_row( 'SELECT `journalID`, `journalServer`, `journalUser`, `journalPass`, `journalComm`, `journalUse`, `journalPics`, `journalPicDefault` FROM `'. $wpdb->jpmirrors .'` WHERE journalID = \''. $_GET['jID'] .'\';' );

  // check to see whether we've got any userpics defined, if we don't, try and grab them from the server
  // or we can force this, if we like
  if( empty( $j->journalPics ) || $_GET['getPics'] == '1' ){
  // attempt to connect to each server to collect data
    $ljc = new DWClient( $j->journalUser, $j->journalPass, $j->journalServer );
	  $response = $ljc->login();

    if( $response[0] === TRUE ) {
      $rsp = $response[1];
      // decode the XML response into something useful
      // best values are $rsp['usejournals'], $rsp['pickws'], $rsp['pickwurls'] & $rsp['friendgrounds']
      //$rsp = php_xmlrpc_decode( $response->value() );
      $error = '';
      $piclist = !empty( $rsp['pickws'] ) ? redonp_getUserPics( $rsp['pickws'] ) : '';
      // if we got something, update the db...
      if( $piclist ) {
        if( !empty( $rsp['defaultpicurl'] ) )
          $j->journalPicDefault = redonp_getDefaultPic( $rsp['pickws'], $rsp['pickwurls'], $rsp['defaultpicurl'] );
        $wpdb->update(
          $wpdb->jpmirrors,
          array( 'journalPics' => $piclist, 'journalPicDefault' => $j->journalPicDefault ),
          array( 'journalID' => $j->journalID ),
          array( '%s', '%s' ),
          array( '%d' )
        );
        $j->journalPics = $piclist;
      }
    } else {
      // if it borkes up at this stage, it's not a major drama,
      // however extended journal options will not be avaliable.
      $error = "<strong>XMLRPC Error #$response[2]:</strong> $response[1].";
      $piclist = '';
    }
  }

  $comm = empty( $j->journalComm ) ? '' : " (". __( 'in' ) ." $j->journalComm)";

  if( !empty( $error ) ){
    echo '<div id="message" class="updated fade"><p>', $error, '<br/>';
    $wpdb->show_errors();
    $wpdb->print_error();
    $wpdb->hide_errors();
    echo '</p></div>';
  }
?>

  <h3><?php _e( 'Edit Journal ', JXP_DOMAIN ); echo $j->journalUser, ' @ ', $j->journalServer, $comm; _e( ' (<a href="./admin.php?page=redonpress">back</a>)' );?></h3>
  <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
  <table class="form-table">
  <tr valign="top">
    <th scope="row"><?php _e( 'Change Password', JXP_DOMAIN ); ?></th>
    <td>
      <input name="redonp_pword" type="password" id="redonp_pword" size="40" />
      <p class="setting-description"><?php _e( 'Leave this field blank to keep the current password.', JXP_DOMAIN ); ?></p>
    </td>
  </tr>
  <tr valign="top">
    <th width="33%" scope="row"><?php _e( 'Use by Default', JXP_DOMAIN ); ?></th>
    <td>
      <label><input name="redonp_use" type="radio" value="yes"<?php echo $j->journalUse == 'yes' ? ' checked="checked"' : ''; ?> /> <?php _e( 'Yes, mirror to this journal by default.', JXP_DOMAIN ); ?></label><br />
      <label><input name="redonp_use" type="radio" value="no"<?php echo $j->journalUse == 'no' ? ' checked="checked"' : ''; ?> /> <?php _e( 'No, hide this journal from the mirror list.', JXP_DOMAIN ); ?></label><br />
      <label><input name="redonp_use" type="radio" value="ask"<?php echo $j->journalUse == 'ask' ? ' checked="checked"' : ''; ?> /> <?php _e( 'Ask me if I wish to use this journal, but do not post to it by default.', JXP_DOMAIN ); ?></label>
    </td>
  </tr>

  <!-- userpics -->
  <tr valign="top">
    <th scope="row"><?php _e( 'Userpics', JXP_DOMAIN ); ?></th>
    <td>
      <textarea name="redonp_pics" id="redonp_pics" cols="40" rows="15"><?php echo htmlentities( stripslashes( $j->journalPics ), ENT_COMPAT, 'UTF-8' ); ?></textarea>
      <p class="setting-description"><?php printf(__( '<a href="./admin.php?page=redonpress&jID=%s&getPics=1">Auto refresh</a>.', JXP_DOMAIN ), $j->journalID ); ?></p>
      <p class="setting-description"><?php _e( 'One per line. Note you cannot have <code>\'</code> or <code>"</code> in your userpic keyword.', JXP_DOMAIN ); ?></p>
    </td>
  </tr>
  <tr valign="top">
    <th scope="row"><?php _e( 'Default Userpic', JXP_DOMAIN ); ?></th>
    <td>
      <input name="redonp_dpic" type="text" id="redonp_dpic" size="40" value="<?php echo htmlentities( stripslashes( $j->journalPicDefault ), ENT_COMPAT, 'UTF-8' ); ?>" />
    </td>
  </tr>
  </table>

  <p class="submit">
    <input type="submit" name="edit_journal" value="<?php _e( 'Edit Mirror' ); ?>" style="font-weight: bold;" />
  </p>
  </form>

<?php } ?>
</div>
<?php
}

// the bulk actions page woo!
function redonp_display_bulk(){
  global $wpdb;

  //Make a variable for the full list of published entries, for use by the crosspost/delete all functions
  $full_list = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE post_status='publish' AND post_type='post'");

  //If crosspost or delete all buttons have been clicked, run the appropriate 'all' function
  if(isset($_REQUEST['crosspost_all'])) {
	  @set_time_limit(0);
	  redonp_post_all($full_list);

	  //get count of all published entries
    $full = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_status='publish' AND post_type='post'");
    //get count of all crossposted entries
    $posted = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key LIKE '_redonp_xpid_%'");

    if ($posted != $full) { //if published posts do not match crossposted posts, give error message
      echo '<div id="message" class="updated fade"><p>';
		  _e( 'ERROR: Not all entries were crossposted.  You may wish to run Crosspost All again.', JXP_DOMAIN );
		  echo '</p></div>';
	  }
	  else { //if they do all match, give success message.
		  echo '<div id="message" class="updated fade"><p>';
		  _e( 'WordPress entries crossposted successfully!', JXP_DOMAIN );
		  echo '</p></div>';
	  }
  }

  if(isset($_REQUEST['delete_all'])) {
	  @set_time_limit(0);
	  redonp_delete_all($full_list);

	  $check = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key LIKE '_redonp_xpid_%'"); //check postmeta for any instance of _redonp_xpid_anything

	  if ($check != 0 ) { //if there are any left, give an error message
		  echo '<div id="message" class="updated fade"><p>';
		  _e( 'ERROR: Not all mirror entries were deleted. You may wish to run Delete All again.', JXP_DOMAIN );
		  echo '</p></div>';
	  }
	  else { //otherwise, give a success message
		  echo '<div id="message" class="updated fade"><p>';
		  _e( 'Mirror entries deleted successfully!', JXP_DOMAIN );
		  echo '</p></div>';
	  }
}

?>
<div class="wrap">
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<h2><?php _e( 'Bulk Actions', JXP_DOMAIN ); ?></h2>
    <p><?php _e('<strong>Do not navigate away from this page while bulk actions are processing.</strong> These functions are set to take five second breaks every fifty entries to keep from hammering the mirror\'s server and causing the function to fail halfway through.  It will take a little while.')?></p>
    <p><?php _e('Errors sometimes occur when dealing with large numbers of entries or high traffic at the server.  If you receive an error message, try running the action again.  This usually takes care of any stragglers.')?></p>

    <table class="form-table">
    <tr>
		  <th><?php _e( 'Crosspost All WordPress Entries', JXP_DOMAIN ); ?></th>
		  <td class="submit"><input type="submit" name="crosspost_all" value="<?php _e('Crosspost All', JXP_DOMAIN); ?>" /><p class="setting-description"><?php _e('Bulk crossposting will only be done to mirrors you set to "use by default" in the JP Mirrors menu.')?></p></td>
		</tr>
		<tr>
		  <th><?php _e( 'Delete All Mirror Entries', JXP_DOMAIN ); ?></th>
		  <td class="submit"><input type="submit" name="delete_all" value="<?php _e('Delete All', JXP_DOMAIN); ?>" /></td>
    </tr>
  </table>

	</form>
</div>
<?php
}

// the extra options on the post page
function redonp_post_advanced() {
  global $wpdb;

  // control variable
  echo '<input type="hidden" name="redonp_isform" value="1"/>';

  // get our list of journals
  $js = $wpdb->get_results( "SELECT journalID, journalUser, journalServer, journalComm, journalUse, journalPics, journalPicDefault FROM $wpdb->jpmirrors WHERE journalUse != 'no' ORDER BY journalServer, journalUser, journalComm;" );
  $xpto = get_post_meta( $_GET['post'], '_redonp_xpto', true );
  $xpto = is_array( $xpto ) ? $xpto : unserialize( $xpto );

  if( !is_array( $xpto ) )
    $xpto = array();

  // do we have some previously saved pic data?
  $tmppics = get_post_meta( $_GET['post'], '_redonp_jpics', true );
  if( $tmppics ){
    $tpa = $tmppics;
  }

  foreach( $js as $js ){
    // check if we're editing or posting new, and tag as appropriate
    $jmeta = get_post_meta( $_GET['post'], '_redonp_xpid_'. $js->journalID, true );
    $jmeta = is_array( $jmeta ) ? $jmeta : unserialize( $jmeta );

    if( empty( $jmeta ) ) {
      $juse = ( $js->journalUse == 'yes' && empty( $_GET['post'] ) && empty( $xpto ) ) ? ' checked="checked"' : '';
      $jword = __( 'Post to ', JXP_DOMAIN );
      $jurl = empty( $js->journalComm ) ? 'http://'. $js->journalServer .'/users/'. $js->journalUser : 'http://'. $js->journalServer .'/community/'. $js->journalComm;
    } else {
      $juse = ' checked="checked"';
      $jword = __( 'Edit in ', JXP_DOMAIN );
      $jurl = $jmeta['url'];
    }

    $jname = empty( $js->journalComm )
           ? '<a href="'. $jurl .'" title="View Journal">'. str_replace( 'www.', $js->journalUser .'.', $js->journalServer ) .'</a>'
           : '<a href="'. $jurl .'" title="View Community">'. str_replace( 'www.', $js->journalComm .'.', $js->journalServer ) .'</a> as <a href="http://'. $js->journalServer .'/users/'. $js->journalUser .'" title="View Journal">'. $js->journalUser .'</a>';

    // get our userpic list
    $picd = empty( $jmeta['jpic'] ) ? $js->journalPicDefault : $jmeta['jpic'];
    $picd = empty( $tpa[$js->journalID] ) ? $picd : $tpa[$js->journalID];
    $piclist = redonp_getUserPicList( $js->journalID, $js->journalPics, $picd );

    echo '<p><label class="selectit"><input type="checkbox" name="jmirrors[]" value="', $js->journalID ,'"', $juse ,' /> ', $jword ,' ', $jname ,'.</label></p>';
    if( $error )
      echo '<p style="margin-left: 3em;">', $error ,'</p>';
    if( $piclist )
      echo '<p style="margin-left: 3em;"><strong>Userpic:</strong> ', $piclist ,'</p>';

    unset( $ljc );
  }
  if( empty( $js ) )
    _e( 'You are not currently mirroring to any journals! Perhaps you\'d like to <a href="./admin.php?page=redonpress">add some</a>?' );
}

/**
 * Add OpenID options to the WordPress discussion settings page.
 */
function redonp_discussion_settings() {
?>
	<fieldset>
		<label for="redonp_enable_commentform">
			<input type="checkbox" name="redonp_require_commentform" id="redonp_require_commentform" value="1" <?php
				echo checked(true, get_option('redonp_require_commentform'));  ?> />
			<?php _e('Require OpenID Auth for comments', 'redonpress') ?>
		</label>
	</fieldset>
<?php
}
