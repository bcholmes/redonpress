<?php
/**
 * @author         BC Holmes
 * @copyright      2017 BC Holmes
 * @license        GPLv2
 */
 
function redonp_comment_form_fields( $fields ) {

	if (get_option('redonp_require_commentform')) {
		if (!get_option( 'require_name_email' )) {
			unset($fields['email']);
		}
		unset($fields['url']);

		$fields['url']  = '<p class="comment-form-url redonp-open-id"><label for="url">' . __( '<span class="open-id-name">OpenID</span> URL' ) . ' <span class="required">*</span>' .'</label> ' .
					'<input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" maxlength="200" class="redonp-openid-url" />' .
					'<span id="redonpress-url-check-result" style="display: none"></span></p>';
	}
	return $fields;
}

function redonp_comment_form_defaults( $defaults ) {

	if (get_option('redonp_require_commentform')) {
		$text = $defaults['comment_notes_before'];
		unset($defaults['comment_notes_before']);
		
		$required_text = sprintf( ' ' . __('Required fields are marked %s'), '<span class="required">*</span>' );
		if (!get_option( 'require_name_email' )) {
			$defaults['comment_notes_before']  = '<p class="comment-notes">' . __( 'Comment using an <span class="openid-name">OpenId</span> account such as Dreamwidth.' ) . '</p>'. $text;
		} else {
			$defaults['comment_notes_before']  = '<p class="comment-notes">' . __( 'Comment using an <span class="openid-name">OpenId</span> account such as Dreamwidth.' ) . $required_text . '</p>';
		}
	}
	return $defaults;
}

function redonp_check_openid_domain() {
	$url = $_POST['url'];
	if (strtolower(mb_substr($url, 0, 4)) !== 'http') {
		$url = 'https://' . $url;
	}
	$parts = parse_url($url);
	$host = $parts['host'];
	if (!preg_match("/[a-z]/i", $host)) {
		wp_send_json_error('Please use a domain name, not an IP address');
	} else {
		wp_send_json_success('Looks good.');
	}
}

 ?>
