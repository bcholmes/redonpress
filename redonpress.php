<?php
/*
Plugin Name: RedonPress
Plugin URI: https://bitbucket.org/bcholmes/redonpress
Description: Mirrors your WordPress blog to any number of LiveJournal-based external journalling sites. Supports per-journal-per-post userpics and custom permission levels via the separate WP-Flock plugin.
Version: 0.5.0
Author: BC Holmes (based on work by Alis Dee)
Author URI: http://blog.bcholmes.org

	Copyright (c) 2008-2016 Alis Dee, BC Holmes

	Permission is hereby granted, free of charge, to any person obtaining a
	copy of this software and associated documentation files (the "Software"),
	to deal in the Software without restriction, including without limitation
	the rights to use, copy, modify, merge, publish, distribute, sublicense,
	and/or sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
	DEALINGS IN THE SOFTWARE.
	
	yay for debugging:  mail( 'email', 'subject', print_r( get_defined_vars(), true ) ); 
*/

define( 'REDONP_PLUGIN_REVISION', '0_5_0' );

if( !defined( 'JXP_DOMAIN' ) )
  define( 'JXP_DOMAIN', '/ljxp/lang/ljxp' );
if( function_exists( 'load_plugin_textdomain' ) )
  load_plugin_textdomain( JXP_DOMAIN );

if( !defined( 'JPDIR' ) )
  define( 'JPDIR', dirname(__FILE__) );

if( defined( 'ABSPATH' ) ){
  require_once( ABSPATH . '/wp-includes/class-IXR.php' );
  require_once( ABSPATH . '/wp-includes/class-wp-http-ixr-client.php' );
  //if( version_compare( $wp_version, "2.3", "<" ) ) 
    //require_once( ABSPATH . '/wp-includes/link-template.php' );
}

// include our other jp files
	include_once( JPDIR . '/redonpinstall.php' );
	include_once( JPDIR . '/lj.class.php' );
	include_once( JPDIR . '/redonpconfig.php' );
	include_once( JPDIR . '/redonpfunctions.php' );
	include_once( JPDIR . '/redonpcomments.php' );

//** INITIAL STUFFS **************************************************//
// add the extra databases 
$wpdb->jpmirrors = $wpdb->prefix . 'redonp_journals';

// activation and deactivation
register_activation_hook( __FILE__, 'redonp_install' );
register_deactivation_hook( __FILE__, 'redonp_uninstall' );

add_action('wp_enqueue_scripts', 'redonp_include_js');

// menu pages
add_action( 'admin_init', 'redonp_admin_register_settings' );
add_action( 'admin_menu', 'redonp_add_pages' );
add_filter( 'plugin_action_links_'. plugin_basename(__FILE__), 'redonp_links' );

// initial options
add_option( 'redonp_installed' );

// hookit!
add_action( 'save_post', 'redonp_save', 5 );
add_action( 'save_post', 'redonp_post', 10 );
//add_action( 'publish_post', 'redonp_post' );

add_action( 'edit_post', 'redonp_edit' );
add_action( 'delete_post', 'redonp_delete' );

add_filter ('the_content', 'redonp_append_links_to_content');

add_filter('comment_form_default_fields', 'redonp_comment_form_fields');
add_filter('comment_form_defaults', 'redonp_comment_form_defaults');

add_action('wp_ajax_redonp_check_openid_domain', 'redonp_check_openid_domain');
add_action('wp_ajax_nopriv_redonp_check_openid_domain', 'redonp_check_openid_domain');

?>