jQuery(function() {
	var redonp_url = jQuery('#url');
	var redonp_submit = jQuery('#submit');
	redonp_submit.prop('disabled', true);
	redonp_url.data('openid', 'false');
	
	if ( redonp_url.val() ) {
		redonp_check_openid_domain( redonp_url );
	}
	redonp_url.blur( function() { redonp_check_openid_domain(jQuery(this)); } );
	
	redonp_submit.click(function() {
		if (redonp_url.data('openid')) {
			return true;
		} else {
			return false;
		}
	});
	
	
	
	function redonp_check_openid_domain(url) {
		var resultField = jQuery('#redonpress-url-check-result');
		resultField.html('Checking URL...');
		resultField.slideDown();
		jQuery.post('/wp-admin/admin-ajax.php', 
			{
				'action': 'redonp_check_openid_domain',
				'url': url.val()
			}, 
			function(response){
				if (response.success) {
					redonp_submit.prop('disabled', false);
					redonp_url.data('openid', 'true');
					resultField.slideUp();
				} else {
					redonp_submit.prop('disabled', true);
					redonp_url.data('openid', 'false');
					resultField.html(response.data);
					resultField.slideDown();
				}
			}
		);
	}

});

