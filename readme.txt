=== RedonPress ===
Contributors: alisdee, bcholmes
License: GPLv3
Tags: livejournal, dreamwidth, crossposting, community, post, posts, social, update
Requires at least: 4.0
Tested up to: 4.5.3
Stable tag: 0.4.0

A cross-poster to Dreamwidth and other LiveJournal-based Servers, including support for multiple servers.

== Description ==

**RedonPress** is a WordPress plugin that enabled cross-posting to sites running LiveJournal Server (i.e. Dreamwidth, LiveJournal, JournalFen, et al.). It is based on the existing LJXP plugin, however it has a raft of new features including:

* Support for multiple different mirror journals;
* Support for custom security groups (via [WP-Flock](http://void-star.net/projects/wp-flock/ "WP-Flock @ void-star.net"));
* Support for scheduled posts;
* Support for posts created from interfaces (i.e. XML-RPC, Atom);
* Mood, music and location support;
* Per-post-per-journal userpic selection;
* Support for cut text; and
* More!

It is currently in its "stable beta" stage, and as such some features may not be available or a little wonky.

The latest updates about the plug-ins development can be found [in the project blog](http://void-star.net/category/geeking/wordpress/journalpress/ "JournalPress @ void-star.net").

= Version 0.3.3 =
* More mucking about with `serialize()`, hopefully now backwards-compatible.

= Version 0.3.2 =
* Fixed the nonsensical use of `serialize()` to work with WordPress v3.0-RC1.

= Version 0.3.1 =
* More sensible backdating.

= Version 0.3 =
* Journal pics, finally!
* Batch updates should hopefully no longer spam f-lists.
* Fixes for bugs: [#2](http://code.google.com/p/journalpress/issues/detail?id=2), [#7](http://code.google.com/p/journalpress/issues/detail?id=7) and [#8](http://code.google.com/p/journalpress/issues/detail?id=8).

= Version 0.2.2 =
* Added an option to allow comments on f-locked crossposts only.
* Made some changes to how the tickyboxes on the post screen behave. Should be more sane now.
* Scheduled posts should now behave a bit better.
* Batch update/delete all posts functionality added, care of [branchandroot](http://www.branchandroot.net/).

= Version 0.2.1 =
* [WP-Flock](http://beta.void-star.net/projects/wp-flock "WP-Flock") compatibility added.
* Interface post support added.
* Some general menu cleaning done to work prettier with WP 2.7.

= Version 0.2 =
* WordPress 2.7 RC1 compatible, yay!
* Re-re-fixed the non-Roman character support. Hopefully for the last time.

= Version 0.1.3 =
* Debugged some of the backdating stuff. It's a bit more brute force now, but hopefully more reliable (maybe).
* Added the ability to tag a post with categories, tags or both (you'll need to re-update your options).
* JournalPress should now accept non-Roman characters in the crosspost text, music and mood field (location and tags still have issues, but I maintain this one is LiveJournal's fault).
* Quotes no longer cause icky backslashes to appear in the mood, music and location fields.
* Thanks to some of the world's most convoluted return code, errors are now no longer silent.

= Version 0.1.2 =
* Scheduled post support added.
* Basic mood, music and location support added.
* Post are now tagged with tags as well as categories (gogo undocumented functions).

== Installation ==

1. Upload the `journalpress` folder to the `/wp-content/plugins/` directory;
1. Activate the plugin through the 'Plugins' menu in WordPress;
1. ...
1. Profit!

== Frequently Asked Questions ==

= Why doesn't x work? =

Because it's a very, very early release of the plug-in, of course! All bugs and suggestions should be filed at [the project issues list](http://code.google.com/p/journalpress/issues/list "JournalPress @ Google Code").

= Where are my userpics? =

Userpics should get automatically added when you add a new mirror journal or community.

If they don't -- or if you've upgraded from a pre-0.3 version of JournalPress -- you may fix the list manually on the edit journal page. If you add or delete any pics in the future, you will also need to come back to this page in order to update your list.

= What about Currents? =

There's current (har har) basic Currents support using the Custom Fields `mood`, `music` and `location` on a post. Note these are case-sensitive, and all lowercase.

This will likely be extended in future to include hooks into other plugins that assist with these features.

= My community posts don't post! =

Communities in LJ-land are a bit finicky. Specifically they won't post if your security is private, and they won't post under certain backdated conditions.

= Do you support MU? =

No. At this time there are no plans to extend support to WordPress MU.

= Why don't you support per-post security settings? =

We do, but it's via a separate plugin: [WP-Flock](http://void-star.net/projects/wp-flock "WP-Flock").

== Credits ==

**JournalPress** is based off the original [LJXP](http://ebroder.net/livejournal-crossposter/) client by Evan Broder, with the [LJ Crossposter Plus](http://www.alltrees.org/Wordpress/#LCP) modifications made by Ravenwood and Irwin. No disrespect is intended towards any of these authors; without their great work, this plugin wouldn't have been possible (or at least would've taken a hell of a lot longer to write).

Batch update and delete journal code care of [branchandroot](http://www.branchandroot.net/).