<?php
//** BACKEND FUNCTIONS ***********************************************//
//** USERPIC STUFF ***************************************************//

defined('REDONP_DIR') or define('REDONP_DIR', __DIR__);


function redonp_getUserPics( $p ){
  foreach( $p as $k => $v ) {
//    if( !preg_match( '/["\']/', $v ) )
    $str .= "$v\n";
  }
  return trim( $str );
}
// get the description of the default userpic, from the url
function redonp_getDefaultPic( $p, $u, $d ){
  foreach( $p as $k => $v ) {
    if( $u[$k] == $d && ( !empty( $u ) && !empty( $d ) ) )
      return $v;
  }
}
// $js->journalID, $js->journalPics, $picd
function redonp_getUserPicList( $n, $p, $d ){
  $parr = explode( "\n", $p );
  $str  = "<select name=\"jpic[$n]\" id=\"jpic". $n ."\" class=\"inline\">\n";
  foreach( $parr as $v ) {
    $sel = trim( html_entity_decode( $v, ENT_QUOTES, 'UTF-8' ) ) == trim( html_entity_decode( $d, ENT_QUOTES, 'UTF-8' ) ) && ( !empty( $v ) && !empty( $d ) ) ? ' selected="selected"' : '';
    $str .= "  <option". $sel .">". htmlentities( stripslashes( $v ), ENT_COMPAT, 'UTF-8' ) ."</option>\n";
  }
  $str .= "</select>\n\n";

  return $str;
}

//** FORMAT A POST ***************************************************//
function redonp_post( $post_id, $xp_set = false ){
	global $wpdb; //need this to get journal ids

	//If this is happening because a post is made, get $xpto from that entry's data
	if( $xp_set === false ){
	  $xpto = get_post_meta( $post_id, '_redonp_xpto', true );
    $xpto = is_array( $xpto ) ? $xpto : unserialize( $xpto );

    // if we're not actually crossposting, we don't need this (hooray)
    if( empty( $_POST['jmirrors'] ) && empty( $xpto ) )
      return $post_id;

    $xpto = $_POST['redonp_isform'] == '1' ? $_POST['jmirrors'] : $xpto;
	} //end xp_set is false

	//if this is happening because of crosspost-all, get the journalids for default-use journals from the redonp_journals table and make that $xpto
	else
		{ $xpto = $wpdb->get_col( "SELECT `journalID` FROM `". $wpdb->jpmirrors ."` WHERE `journalUse` = 'yes';" ); }
	//end xp_set is true (set in the 'all' function call)

  // so we're only hitting this once in the case of multiple cross-posts,
  // get the post itself
	$post = & get_post( $post_id );
	// there's a couple of reasons we wouldn't publish,
	// if we're hitting any of those, pull out
	if( ( $post->post_status != 'publish' && $post->post_status != 'private' ) || $post->post_type != 'post' )
	  return $post_id;

  // if that's good, get our relevant details from the database...
  $opts = array(
    'redonp_privacy'       => get_option( 'redonp_privacy' ),
    'redonp_privacyl'      => get_option( 'redonp_privacyl' ),
    'redonp_privacyp'      => get_option( 'redonp_privacyp' ),
	  'redonp_comments'      => get_option( 'redonp_comments' ),
	  'redonp_tag'           => get_option( 'redonp_tag' ),
	  'redonp_cat'           => get_option( 'redonp_cat' ),
	  'redonp_more'          => get_option( 'redonp_more' ),
	  'redonp_header_loc'    => get_option( 'redonp_header_loc' ),
	  'redonp_custom_header' => stripslashes( get_option( 'redonp_custom_header' ) ),
	  'redonp_custom_name'   => stripslashes( get_option( 'redonp_custom_name' ) ),
    'redonp_bulk'          => $xp_set
	);

  // else, for each of the elements in jmirrors, we need to format and cross-post a post
  $r = array();
  if( is_array( $xpto ) ) {
    $pics = get_post_meta( $post_id, '_redonp_jpics', true );
    $jpic = $_POST['redonp_isform'] != '1' && !empty( $pics ) ? $pics : $_POST['jpic'];
    foreach( $xpto as $jID ) {
      if( $rt = redonp_format_post( $jID, $post, $opts, $jpic[$jID] ) )
        $r[] = $rt;
    }
  }
  // now clean up our xpto meta (since we should have proper return values now)
  //delete_post_meta( $post_id, '_redonp_xpto' );
  //delete_post_meta( $post_id, '_redonp_jpics' );

  // did we get any errors?
  // this is kinda an icky weird way of doing this, but oh well...
  if( !empty( $r ) ){
    $str = "<strong>Oh Gnoes! Errors!</strong>";
    foreach( $r as $e )
      $str .= "\n<p>$e[0]: $e[1]</p>";
    wp_die( $str );
  }

  return $post_id;
}

function redonp_format_post( $jID, $p, $o, $pic ){
  global $wpdb;

  // okay, get our journal details from the db
	$j = $wpdb->get_row( "SELECT `journalServer`, `journalUser`, `journalPass`, `journalComm` FROM `". $wpdb->jpmirrors. "` WHERE journalID = '$jID';" );

	// you can't have private posts in a community, so let's not try
  if( $p->post_status == 'private' && !empty( $j->journalComm ) )
    return;

  // if we've got a private post, and we don't crosspost those, return
  // note that posts locked with WP-Flock are marked private to, and we may want to crosspost
  // those, so check for a umask
  $pmask = jpfl_get_umask( $p->ID );
  if( $p->post_status == 'private' && empty( $p->post_password ) && $o['redonp_privacyp'] == 'noxp' )
    return;
  if( $p->post_status == 'private' && $pmask == -1 )
    return;

  // if we've got a password-protected post, and we don't crosspost those, return
  if( !empty( $p->post_password ) && $o['redonp_privacyl'] == 'noxp' )
    return;

	// create our DWClient class, but don't do anything with it yet
  $ljc = new DWClient( $j->journalUser, $j->journalPass, $j->journalServer, $j->journalComm );

  // start formatting!
  $the_event = '';
	// Segment to add replacement of --cut-- with lj-cut plus rewriting, by Emily Ravenwood and Ken Irwin.
		//horsing around to define whether there's a cut tag
		$cut_present = ( strpos( $p->post_content, "<!--/cut-->" ) === false )
					 ? false
					 : true;

		//and whether there's a more tag
		$more_present = ( strpos( $p->post_content, "<!--more-->" ) === false )
					 ? false
					 : true;

		//if there's a cut
		if( $cut_present === true ){
			//Rename content so it doesn't mess up the logic later
			$content_string = $p->post_content;

			// set the condition for the loop so we can replace multiple cut tags
			// kinda nasty
			$processing = true;
			$n = preg_match_all( '#<!--[ ]*/cut[ ]*-->#m', $content_string, $n_tmp );

			while( $processing === true ){
				// match the string of Scott Reilly's hide-or-cut-tag plugin and extract the parts
				// #^(.*)<!--[ ]*cut[ ]*=?([^\-]*)-->(.*)<!--[ ]*/cut[ ]*-->(.*)#ms
				if( preg_match( '#^(.*)<!--[ ]*cut[ ]*=?["]?([^"\-]*)["]?-->(.*)<!--[ ]*/cut[ ]*-->(.*)#ms', $content_string, $m ) ) {
					list( $whole, $pre, $cut_text, $cut_content, $rest ) = $m;

					//working backwards from the end, put lj-cut tags around the parts
					//$processed_text = '<lj-cut text="' . $cut_text . '">' . $cut_content . '</lj-cut>' . $rest. $processed_text;
					switch( $o['redonp_more'] ){
						case 'copy': // this doesn't make much sense but oh well...
							$processed_text = $cut_content . $rest . $processed_text;
							break;
						case 'link':
						  $cut_text = ( empty( $cut_text ) ) ? 'Read more...' : $cut_text;
							$processed_text = sprintf( '<p><strong>( <a href="%s#cut-%s">', get_permalink( $p->ID ), $n ) . $cut_text .'</a> )</strong></p>'. $rest . $processed_text;
							break;
						case 'lj-cut':
							$processed_text = '<lj-cut text="' . $cut_text . '">' . $cut_content . '</lj-cut>' . $rest. $processed_text;
							break;
						default:
							$processed_text = $cut_content . $rest . $processed_text;
							break;
					}

					$n--;
					$content_string = $pre;
				} else
					$processing = false;
			} // endwhile

			// and here's our event to pass on to the rest of the crosspost plugin
			$the_event .= apply_filters( 'the_content', $pre . $processed_text );
		} // end --cut-- replace segment

		// if there is a --more-- tag, process that
		elseif ($more_present == true) {
			$content = explode("<!--more-->", $p->post_content, 2);
			$the_event .= apply_filters('the_content', $content[0]);
			switch($o['redonp_more']) {
			case "copy":
				$the_event .= apply_filters('the_content', $content[1]);
				break;
			case "link":
				$the_event .= sprintf('<p><a href="%s#more-%s">', get_permalink($p->ID), $p->ID) .
					__('Read the rest of this entry &raquo;', JXP_DOMAIN) .
					'</a></p>';
				break;
			case "lj-cut":
				$the_event .= '<lj-cut text="' .
					__('Read the rest of this entry &amp;raquo;', JXP_DOMAIN) .
					'">' . apply_filters('the_content', $content[1]) . '</lj-cut>';
				break;
			default:
				$the_event .= apply_filters('the_content', $content[1]);
				break;
			}
		} //end --more--section

		//if niether cut tags nor more tags are being used...
		elseif ( ($cut_present == false) && ($more_present == false) ) {
			$the_event .= apply_filters('the_content', $p->post_content);
		}//end if no tags
	// end that bit, yay for other peoples' code

  // format the post header/footer
  // insert the name of the page we're linking back to based on the options set
	$blogName = empty( $o['redonp_custom_name'] ) ? get_option( "blogname" ) : $o['redonp_custom_name'];
  $postHeader = $o['redonp_custom_header'];
  $find = array( '[blog_name]', '[blog_link]', '[permalink]', '[comments_link]' );
  $replace = array( $blogName, get_settings( 'home' ), get_permalink( $p->ID ), get_permalink( $p->ID ).'#comments' );
  $postHeader = str_replace( $find, $replace, $postHeader );

	// Either prepend or append the header to $the_event, depending on the
	// config setting
	// Remember that 0 is at the top, 1 at the bottom
	if( $o['redonp_header_loc'] )
		$the_event .= $postHeader;
	else
		$the_event = $postHeader . $the_event;

  // Retrieve the categories that the post is marked as - for LJ tagging
  // wtf is the WP function to return tags?
  $cat_names = array();
  if( $o['redonp_cat'] ){
  	$cats = wp_get_post_cats( '', $p->ID );
  	// I need them in an array for my next trick to work
  	if( !is_array( $cats ) )
  		$cats = array( $cats );

  	// Convert the category IDs of all categories to their text names
  	$cat_names = array_map( "get_cat_name", $cats );
  }
	// now for the tags
	// this is crazy undocumented, so thanks to:
	// <http://www.sitepoint.com/forums/showthread.php?t=576655>
  $tags = array();
  if( $o['redonp_tag'] ){
    $existing_tags = wp_get_post_tags( $p->ID );
    if (count($existing_tags) > 0) {
      foreach ($existing_tags as $tag) {
        if ($tag->taxonomy == 'post_tag')
          $tags[] = $tag->name;
      }
    }
  }
	// Turn them into a comma-seperated list for the API
	$cat_string = implode( ", ", array_merge( $tags, $cat_names ) );

  // Get a timestamp for retrieving dates later
	$date = strtotime( $p->post_date );

  // start building our data to post
	$jdata = array();

	// The filters run the WP texturization - cleans up the code
	$jdata['event'] = $the_event;
	$jdata['subject'] = apply_filters( 'the_title', $p->post_title );

	// All of the relevent dates and times
	$jdata['year'] = date( 'Y', $date );
	$jdata['mon'] = date( 'n', $date );
	$jdata['day'] = date( 'j', $date );
	$jdata['hour'] = date( 'G', $date );
	$jdata['min'] = date( 'i', $date );

	// Set the privacy level according to the settings
	$pwhat = $o['redonp_privacy'];
	if( $p->post_status == 'private' )
	  $pwhat = $o['redonp_privacyp'];
	if( $p->post_status == 'private' && $pmask > 0 )
	  $pwhat = 'usemask';
	if( !empty( $p->post_password ) )
	  $pwhat = $o['redonp_privacyl'];

	switch( $pwhat ){
  	case 'public':
  		$jdata['security'] = 'public';
  		break;
  	case 'private':
  		$jdata['security'] = 'private';
  		break;
  	case 'friends':
  		$jdata['security'] = 'usemask';
  		$jdata['allowmask'] = 1;
  		break;
  	case 'usemask':
  		$jdata['security'] = 'usemask';
  		$jdata['allowmask'] = $pmask;
  		break;
    default:
      $jdata['security'] = 'private';
  		break;
	}

  // figure out if we're allowed to comment
  $no_comment = 1;
  if( ( $o['redonp_comments'] == 1 ) || ( $o['redonp_comments'] == 2 && $pwhat != 'public' ) )
    $no_comment = 0;

  // build our props
	$jmeta = array(
    // enable or disable comments as specified by the
	  //"opt_nocomments" => !$o['redonp_comments'],
	  'opt_nocomments' => $no_comment,
    // Tells LJ to not run it's formatting (replacing \n
    // with <br>, etc) because it's already been done by
    // the texturization
    'opt_preformatted' => true,
    // userpic!
    'picture_keyword' => $pic
  );

  // if we're bulk-updating this, we want to backdate (so we're not spamming our friends)
  if( $o['redonp_bulk'] === true )
    $jmeta['opt_backdated'] = 1;

	// If tagging is enabled...
	if( $o['redonp_tag'] || $o['redonp_cat'] )
		$jmeta['taglist'] = stripslashes( $cat_string );

  // basic mood, music and location support, just the facts ma'am
  if( $loc = get_post_meta( $p->ID, 'location', true ) )
    $jmeta['current_location'] =  stripslashes( $loc );
  if( $mood = get_post_meta( $p->ID, 'mood', true ) )
    $jmeta['current_mood'] =  stripslashes( $mood );
  if( $music = get_post_meta( $p->ID, 'music', true ) )
    $jmeta['current_music'] =  stripslashes( $music );

	// let's do this thing...

  error_log("attempting new esc_sql");
  $parr = array( 'jpic' => esc_sql( htmlentities( $pic, ENT_COMPAT, 'UTF-8' ) ) );

	// check whether we're editing a post or making something new
  // if we are, we want to backdate
	if( $isxp = get_post_meta( $p->ID, '_redonp_xpid_'. $jID, true ) ){
    $isxp = is_array( $isxp ) ? $isxp : unserialize( $isxp );
	  $xp = $isxp;
	  $jdata['itemid'] = $xp['itemid'];
	  $r = $ljc->editevent( $jdata, $jmeta );
    $jmeta['opt_backdated'] = 1;
	  if( $r[0] === TRUE )
  	  update_post_meta( $p->ID, '_redonp_xpid_'. $jID, array_merge( $r[1], $parr ), $isxp );
	} else {
	  $r = $ljc->postevent( $jdata, $jmeta );
	  // we should really do something if the post fails, hey
  	if( $r[0] === TRUE )
  	  add_post_meta( $p->ID, '_redonp_xpid_'. $jID, array_merge( $r[1], $parr ) );
  	elseif( $r[0] === FALSE ){
  	  // if that didn't work, as a last-ditch attempt, try and post backdated if we didn't before
  	  $jmeta['opt_backdated'] = 1;
  	  $r = $ljc->postevent( $jdata, $jmeta );
  	  if( $r[0] === TRUE )
  	    add_post_meta( $p->ID, '_redonp_xpid_'. $jID, array_merge( $r[1], $parr ) );
  	}
	}

	unset( $ljc );
	if( $r[0] === FALSE ) {
	  $comm = !empty( $j->journalComm ) ? " ($j->journalComm)" : '';
	  $r[0] = "<em>$j->journalUser @ $j->journalServer". $comm ."</em>";
	  return $r;
	} else {
	  // now clean up our xpto meta (since we should have proper return values now)
    delete_post_meta( $post_id, '_redonp_xpto' );
    delete_post_meta( $post_id, '_redonp_jpics' );
	  return;
  }
}

//** DELETE A POST ***************************************************//
function redonp_delete( $post_id ){
  global $wpdb;
  // okay, if we're deleting a post, we need to look for -all- journals
  // said post was mirrored to, so...
  // get our list of journals
  $js = $wpdb->get_results( "SELECT journalID, journalUser, journalPass, journalServer, journalComm FROM $wpdb->jpmirrors;" );
  foreach( $js as $js ){
    $isxp = get_post_meta( $post_id, '_redonp_xpid_'. $js->journalID, true );
    $isxp = is_array( $isxp ) ? $isxp : unserialize( $isxp );
    if( !empty( $isxp ) ){
      $xp = $isxp;
      // create our DWClient class, and let's go!
      $ljc = new DWClient( $js->journalUser, $js->journalPass, $js->journalServer, $js->journalComm );
      $r = $ljc->deleteevent( $xp['itemid'] );
      if( $r[0] === TRUE )
        delete_post_meta( $post_id, '_redonp_xpid_'. $js->journalID );

      unset( $ljc );
    }
  }
  // this seems to run at slightly unpredictable times?
  //delete_post_meta( $post_id, '_redonp_xpto' );
  //delete_post_meta( $post_id, '_redonp_jpics' );

  return $post_id;
}

function redonp_edit( $post_id ){
	// This function will delete a post from LJ if it's changed from the published status

	// get the post details
	$post = & get_post( $post_id );

	// See if the post is currently published. If it's been crossposted and its
	// state isn't published, then it should be deleted
	if( ( $post->post_status != 'publish' && $post->post_status != 'private' ) || jpfl_get_umask( $post_id ) == -1 )
		redonp_delete( $post_id );

  // also, if its groups have been changed to "no crosspost" we should also delete it


	return $post_id;
}

function redonp_save( $post_id ){
  global $wpdb;
  // easiest thing first; save our selected journal pics
  redonp_pic_meta( $post_id, $_POST['jpic'] );

  $xpto = get_post_meta( $post_id, '_redonp_xpto', true );
  $xpto = is_array( $xpto ) ? $xpto : unserialize( $xpto );
  // tsh... what we need to do here is figure out a way to set in the meta, even with
  // no return values for scheduled posts
  if( empty( $_POST['jmirrors'] ) && $_POST['redonp_isform'] != '1' && empty( $xpto ) ) {
    // if we haven't specifically selected which journals we want/don't want (i.e. this post is coming in via a remote interface),
    // get the default set and post to those
    $js = $wpdb->get_results( "SELECT journalID FROM `$wpdb->jpmirrors` WHERE journalUse = 'yes';" );
    $jnum = $wpdb->get_var( "SELECT COUNT(*) FROM `$wpdb->jpmirrors` WHERE journalUse = 'yes' GROUP BY journalUse;" );
    if( $jnum > 0 ){
      foreach( $js as $js )
        $jmirrors[] = $js->journalID;
      redonp_post_meta( $post_id, $jmirrors );
    } else
      return $post_id;
  }

  redonp_post_meta( $post_id, $_POST['jmirrors'] );

  return $post_id;
}

//** UPDATE POST META ************************************************//
function redonp_post_meta( $post_id, $jmirrors ){
  if( !$jmirrors )
    return;
  if( $xpto = get_post_meta( $post_id, '_redonp_xpto', true ) ){
    $xpto = is_array( $xpto ) ? $xpto : unserialize( $xpto );
  	update_post_meta( $post_id, '_redonp_xpto', $jmirrors, $xpto );
	} else
  	add_post_meta( $post_id, '_redonp_xpto', $jmirrors, true );
}
function redonp_pic_meta( $post_id, $jpic ){
  if( !$jpic )
    return;
  if( $pics = get_post_meta( $post_id, '_redonp_jpics', true ) )
  	update_post_meta( $post_id, '_redonp_jpics', $jpic, $pics );
	else
  	add_post_meta( $post_id, '_redonp_jpics', $jpic, true );
}

//** WP-FLOCK INTEGRATION ********************************************//
// use this to check if we've got WP-Flock installed, and if so, dig out
// the umask of a crossposted post
function jpfl_get_umask( $post_id ){
  global $wpdb;
  $fl = get_option( 'fl_installed' );
  if( empty( $fl ) || empty( $wpdb->flcaps ) )
    return false;

  // get any jp groups
  $gs = $wpdb->get_results( $wpdb->prepare(
      'SELECT g.`gMask` FROM `'. $wpdb->flcaps .'` AS c JOIN `'. $wpdb->flgroups .'` AS g ON g.gID = c.gID WHERE c.oType = %s AND c.oID = %d;'
      ,'post'
      ,$post_id
    ) );
  // if we don't have anything, return false
  if( empty( $gs ) )
    return false;
  $mask = array();
  foreach( $gs as $g ){
    if( $g->gMask == -1 )
      return -1;
    if( $g->gMask > 0 )
      $mask[] = $g->gMask;
    else
      $flock = 1;
  }
  $mask = redonp_get_bitmask( $mask );
  return $mask > 0 ? $mask : $flock;
}
// make a bitmask for an item
// in this case, $pmask is an array of decimals (1,2,3,...)
// these decimals should be used as the 'powers' for the
// bitmask
function redonp_get_bitmask( $pmask ){
  if( empty( $pmask ) )
    return 0;

  $mask = 0;
  foreach( $pmask as $v )
    $mask |= ( 1 << $v );
    //$mask = $mask | pow( 2, $v );

  return $mask;
}


//** CROSSPOST/DELETE ALL *********************************************//
//Crosspost all
function redonp_post_all($id_list) {
	$i = 0;
	foreach((array)$id_list as $id) {
		$i++;
		if ( $i%50 == 0 ) { //if increment is divisible by 50, take a break
			sleep(5);
		}
		$xp_set = true; //set this for use in the redonp_post function
		redonp_post($id, $xp_set);
	}
}

//Delete all
function redonp_delete_all($id_list) {
	$i = 0;
	foreach((array)$id_list as $id) {
		$i++;
		if ( $i%50 == 0 ) { //if increment is divisible by 50, take a break
			sleep(5);
		}
		redonp_delete($id);
	}
}

function redonp_append_links_to_content($content) {
	global $wp_query;

	if (is_single() && is_user_logged_in()) {

		$post = $wp_query->post;
		$id = $post->ID;
		$xpost_to = get_post_meta($id, '_redonp_xpto', true);
		foreach ($xpost_to as $key => $value) {
			$xpost_details = get_post_meta($id, '_redonp_xpid_'.$value, true);
			$url = $xpost_details['url'];
			if (strlen($url) > 0) {
          $content.='<div class="redon-crosspost-links"><div>Crossposts:</div><ul>';
			    $content.='<li><a href="'.$url.'">'.$url.'</a></li>';
          $content.='</ul></div>';
			}
		}
	}
	return $content;
}

function redonp_include_js() {
	if (get_option('redonp_require_commentform')) {
		if ( ( is_singular() && ( have_comments() || comments_open() ) ) || is_admin() ) {
			wp_enqueue_script( 'redonpress', plugin_dir_url( __FILE__ ) . 'assets/redonpress.js', array( 'jquery' ), REDONP_PLUGIN_REVISION );
		}
	}
	wp_register_style( 'redonpress', plugin_dir_url( __FILE__ ) . 'assets/redonpress.css' );
	wp_enqueue_style( 'redonpress' );
}


?>
